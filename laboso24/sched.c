/*
 * sched.c - initializes struct for task 0 anda task 1
 */
#include <libc.h>
#include <sched.h>
#include <list.h>
#include <mm.h>
#include <io.h>
#include <utils.h>

struct task_struct * idle_task; 
unsigned long getEbp();
union task_union task[NR_TASKS]
  __attribute__((__section__(".data.task")));

int ticks; 


struct list_head readyqueue,freequeue; 

#if 1
struct task_struct *list_head_to_task_struct(struct list_head *l)
{
  return list_entry( l, struct task_struct, list);
}
#endif

extern struct list_head blocked;


/* get_DIR - Returns the Page Directory address for task 't' */
page_table_entry * get_DIR (struct task_struct *t) 
{
	return t->dir_pages_baseAddr;
}

/* get_PT - Returns the Page Table address for task 't' */
page_table_entry * get_PT (struct task_struct *t) 
{
	return (page_table_entry *)(((unsigned int)(t->dir_pages_baseAddr->bits.pbase_addr))<<12);
}


//update el nombre de ticks que el procés ha executat desde que ha entrat a la cpu 
void update_sched_data_rr(){
	
	ticks--; 
	struct stats * sta = &current()->stats; 
	sta->remaining_ticks = ticks; 

}

/*decide if a context switch is needed :
-quantum is over and there are some candidate process to use the cpu*/
/*idle process if there's no other process that can advance the execution*/
//retorna 1 si fa falta canviar el current process i 0 si no 
int needs_sched_rr(void){
	//s'han de mirar més coses1111
       // printk("needs_sched\n");
	if(ticks <= 0  && !list_empty(&readyqueue)){	//s'ha acabat el quantum i no estem a init
		return 1; 
	} 
	if(ticks == 0) ticks = 20;
	else return 0;	//si no 

}


/*update el current state del procés a un altre state, 
1- esborrar el procés de la ready queue
2- l'inserta en una nova queue (qualsevol)
3. si el current state és running, no hi ha necessitat d'esborrarlo de alguna queue
(els paràmetres són el task_struct del procés i la nova queue, si el nou estat és running la queue ha de ser null)*/
void update_process_state_rr(struct task_struct *t, struct list_head *dst_queue){
	//mirem el estat, si no és run l'hem d'esborrar de ninguna queue 
        //printk("update");	
	if(t->state != 1){
		list_del(&(t->list)); 
	}

	//inserta el procés a la nova queue I FER UPDATE DEL STATE
	//dst_queue = readyqueue or freequeue, a la freequeue no pot ser perquè és al delete process
	//si no va a la readyqueue és que està blocked -> posar state blocked
	//si va a la readyqueue-> encuar a la readyqueue i posar stat ready 
	//si és null, és que va a run, només canviar estat
		
	if(dst_queue == &readyqueue){
		list_add_tail(&(t->list),&readyqueue);
		t->state = 0;
	} else if (dst_queue == NULL ) {
		t->state = 1;
	} else { 
		t->state = 3; 
		list_add_tail(&(t->list),dst_queue);
	}
        //printk("update_fora");	
}

/*select el següent procés a executar, el treu de la ready queue i invoca al context switch
sempre cridat després del update_process_state_rr*/


int get_quantum(struct task_struct *t){
	return t->quantum;

}

void set_quantum (struct task_struct *t, int new_quantum){
	t->quantum = new_quantum;

}


void sched_next_rr(){	
	//printk("sched_next\n");
	if(list_empty(&readyqueue)){
	//printk("bon dia\n");
		task_switch((union task_union*) idle_task);

	}else{
		//printk("adeu\n");
		struct list_head * next = list_first(&readyqueue);
		list_del(next);
		struct task_struct * task_struct_next;
		task_struct_next = list_head_to_task_struct(next);	
		ticks = get_quantum(task_struct_next);
		
		struct stats * sta = &task_struct_next->stats; 
		sta->total_trans++; 
		sta->ready_ticks += get_ticks() - sta->elapsed_total_ticks; 
		sta->elapsed_total_ticks = get_ticks(); 
		
		task_switch((union task_union*) task_struct_next);  
	}	
}
	



void sched(){
        //printk("hola\n");	
	update_sched_data_rr();
	if (needs_sched_rr()){ //si s'ha acabat el quantum i necessitem context switch 
		update_process_state_rr(current(), &readyqueue);
		sched_next_rr(); 
	} /*else if (list_empty()){
		task_switch((union task_union*) idle_task); 
	} */ 

}


int allocate_DIR(struct task_struct *t) 
{
	int pos;

	pos = ((int)t-(int)task)/sizeof(union task_union);

	t->dir_pages_baseAddr = (page_table_entry*) &dir_pages[pos]; 

	return 1;
}

void cpu_idle(void)
{
	__asm__ __volatile__("sti": : :"memory");
	printk("He executat idle\n");
	//sys_fork();
	while(1)
	{
	printk("while!!!!!!!!!!!!"); 
	
	}
}

void init_idle (void)
{
	struct list_head *e = list_first(&freequeue); /*primer element de la freequeue*/
	list_del(e);  /*l'esborrem just in case no es lii*/

	idle_task  = list_head_to_task_struct(e); /*intentem rescatar el task struct del que he										m tret*/
	idle_task->PID = 0; /*assignem pid = 0*/
	/*coses que fiquem pel round robin*/
	idle_task->quantum = 100; //state no fa falta 
	

	allocate_DIR(idle_task); /*punt 3*/
	
	/*punt 4*/
	/*creem el task union per poder tenir el stack del sistema*/
	union task_union * idle_union = (union task_union*) idle_task; 


	/*li fiquem el 0 pel pop ebp*/ /*!!!!!!!!!!!!! potser va al revéś*/
	idle_union->stack[KERNEL_STACK_SIZE-2] = (unsigned long) 0; 
	idle_union->stack[KERNEL_STACK_SIZE-1] = (unsigned long) cpu_idle;
	

	/*moure el kernel_ebp per que apunti a la pila de sistema*/
	idle_union->task.kernel_esp = (unsigned long) &idle_union->stack[KERNEL_STACK_SIZE-2];
	
	
	printk("idle!!!!!!!!!!!!\n"); 


}

void init_task1(void)
{
	printk("init!!!!!!!!!!!!\n"); 
	struct list_head *e = list_first(&freequeue); /*primer element de la freequeue*/
	list_del(e);  /*l'esborrem just in case no es lii*/
   
	struct task_struct *init_task  = list_head_to_task_struct(e); /*intentem rescatar el task struct del que he										m tret*/
	init_task->PID = 1; /*assignem pid = 1*/
	/*coses del rr*/
	init_task->quantum = 30; 
	ticks = 30;
	init_task->state   = 1;
	allocate_DIR(init_task); /*punt 2*/

	set_user_pages(init_task); 
	
	tss.esp0 = (unsigned long) KERNEL_ESP((union task_union *) init_task); /*punt 4*/

	writeMsr(0x175,KERNEL_ESP((union task_union *) init_task));
	set_cr3(init_task->dir_pages_baseAddr); 
	
 			
	//task_switch((union task_union * ) idle_task);

	
}

void inner_task_switch(union task_union *new){
	tss.esp0 = (int)& (new->stack[KERNEL_STACK_SIZE]);
	writeMsr(0x175, (int) &(new->stack[KERNEL_STACK_SIZE]));	
	set_cr3(new->task.dir_pages_baseAddr);
	new->task.state = 1; 

	__asm__ __volatile__(			//3: hem guardat el valor de ebp
		"movl %%ebp, %0\n\t"
		:"=g" (current()->kernel_esp)
		:"g"  (new->task.kernel_esp)
		:);
		

	__asm__ __volatile__(			//4: change current system stack canviant el esp 
		"movl %0, %%esp\n\t"
		:
		:"g" (new->task.kernel_esp)

		);

	__asm__ __volatile__(			//5 
		"popl %%ebp\n\t"
		:
	:);
	
	__asm__ __volatile__(			//6
		"ret"
	:);
	 
	
}

void init_sched()
{
	INIT_LIST_HEAD(&freequeue); /*inicialitzem la frequeue*/
	for(int i = 2; i < NR_TASKS; i++) /*assignem les tasks lliures (que són totes) però comencen al 
					    2 perquè 0 és idle i 1 és init, i arriba fins a NR_TASKS*/

	{
		list_add_tail(&(task[i].task.list), &freequeue);

		/*al list.h posa que el list_add_tail() és el que s'ha de fer servir per construir queues
		  així que ens en refiem*/ 
		/*dins de cada task_union entrem al task_struct i dins d'aquest entra al list per lligar les lists*/
	}
	INIT_LIST_HEAD(&readyqueue); /*inicialització però està empty al beggining*/


}

void es(int a){ //entrada sortida
	struct stats * sta = &current()->stats; 
	if (a == 0){ //entrada
		sta->user_ticks += get_ticks() - sta->elapsed_total_ticks; 
		sta->elapsed_total_ticks = get_ticks(); 

	} else {     //sortida
		sta->system_ticks += get_ticks - sta->elapsed_total_ticks;
		sta->elapsed_total_ticks = get_ticks();	

	}
}


struct task_struct* current()
{
  int ret_value;
  
  __asm__ __volatile__(
  	"movl %%esp, %0"
	: "=g" (ret_value)
  );
  return (struct task_struct*)(ret_value&0xfffff000);
}




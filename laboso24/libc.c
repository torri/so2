/*
 * libc.c 
 */

#include <libc.h>

#include <types.h>

int errno;

void itoa(int a, char *b)
{
  int i, i1;
  char c;
  
  if (a==0) { b[0]='0'; b[1]=0; return ;}
  
  i=0;
  while (a>0)
  {
    b[i]=(a%10)+'0';
    a=a/10;
    i++;
  }
  
  for (i1=0; i1<i/2; i1++)
  {
    c=b[i1];
    b[i1]=b[i-i1-1];
    b[i-i1-1]=c;
  }
  b[i]=0;
}

int strlen(char *a)
{
  int i;
  
  i=0;
  
  while (a[i]!=0) i++;
  
  return i;
}

  
void perror(void) {
  	if(errno == 12) write(1 , "no queda memoria" , 30);
  	if(errno == 13) write(1 , "no tens permisos" , 30);
	if(errno == 14) write(1 , "falta espai al buffer", 30);
	if(errno == 22) write(1,  "mida invàlida" , 30);
  	if(errno == 38) write(1 , "funció no implementada" , 30);
  	if(errno == 77) write(1 , "bad fd" , 30);

}


/*
 * sys.c - Syscalls implementation
 */
#include <devices.h>

#include <utils.h>

#include <io.h>

#include <mm.h>

#include <mm_address.h>

#include <errno.h>

#include <sched.h>

#define LECTURA 0
#define ESCRIPTURA 1

extern int zeos_ticks;
//struct list_head freequeue, readyqueue; 
int check_fd(int fd, int permissions)
{
  if (fd!=1) return -9; /*EBADF*/
  if (permissions!=ESCRIPTURA) return -13; /*EACCES*/
  return 0;
}

int ret_from_fork(){
       	return 0;
}

int sys_ni_syscall()
{
	return -38; /*ENOSYS*/
}

int sys_getpid()
{
	return current()->PID;
}

int sys_fork()
{
  int PID = -1;

//2 -  creates the child process
/*A - Buscar task_struct lliure*/
  if(list_empty(&freequeue)) return -ENOMEM;  
  struct list_head *lliure  = list_first(&freequeue);
  list_del(lliure);
  struct task_struct *fill = list_head_to_task_struct(lliure);


/*B - copy the parent's task_union to the child*/
  copy_data(current(), fill, (int) sizeof(union task_union));  


/*C - allocate la new directory*/
  if(allocate_DIR(fill) < 0) return -ENOMEM; //no hi ha lloc per la tp


/*D i eiiA - buscar lloc per les physical pages i mapejar-les a la TP del pare*/
	//existe el alloc_frame que es para un single frame, hay que hacerlo para datos+pila??
	//tamaño data = NUM_PAG_DATA
  page_table_entry *tp_pare = get_PT(current());
  for(int i = 0; i < NUM_PAG_DATA; ++i){
	int nou_frame;

	if((nou_frame = alloc_frame()) != -1) set_ss_pag(tp_pare,PAG_LOG_INIT_DATA + NUM_PAG_DATA + i, nou_frame);

	else{ //No hi ha mes pagines lliures. Desalocatar tot el que hem alocatat abans
		for(int j = 0; j < i; ++j){
			free_frame(get_frame(tp_pare, PAG_LOG_INIT_DATA + NUM_PAG_DATA + j));
			del_ss_pag(tp_pare, PAG_LOG_INIT_DATA + NUM_PAG_DATA + j);
		}
		list_add_tail(lliure, &freequeue);
		return -EAGAIN;
	}
  }


//ei Create a new address space	       	
  page_table_entry * tp_fill = get_PT(fill);

  
//eiA Diem al fill on són Kernel i sistema del pare(no les copiem peruè són compartides)
  for(int i = 0; i < NUM_PAG_KERNEL; ++i){
	  set_ss_pag(tp_fill, i, get_frame(tp_pare, i));
  }
  for(int i = 0; i < NUM_PAG_CODE; ++i){
	set_ss_pag(tp_fill, i + PAG_LOG_INIT_CODE, get_frame(tp_pare, PAG_LOG_INIT_CODE+i));
  
  }


//eiB Li diem al fill on són les seves pàgines físiques de data+stack
  for(int i = 0; i < NUM_PAG_DATA; ++i){
    int frame = get_frame(tp_pare,PAG_LOG_INIT_DATA + NUM_PAG_DATA + i);
    set_ss_pag(tp_fill, NUM_PAG_KERNEL + NUM_PAG_CODE + i, frame); 
  }


//eiiBC Copy data+stack + buidar TLB pare
for(int i = 0; i < NUM_PAG_DATA; ++i){
     copy_data((void *) ((NUM_PAG_KERNEL + NUM_PAG_CODE + i) << 12), (void *) ((PAG_LOG_INIT_DATA + NUM_PAG_DATA + i) << 12) , PAGE_SIZE);
     del_ss_pag(tp_pare, PAG_LOG_INIT_DATA + NUM_PAG_DATA + i);
   }
  set_cr3(get_DIR(current()));


//f assignem PID disponible al fill
  int pid_fill = current()->PID;
  int existeix = 1;

  while(existeix == 1){
     ++pid_fill;
     existeix = 0;
     for(int i = 0; i < NR_TASKS && existeix == 0 ; ++i){
	     if(task[i].task.PID == pid_fill) existeix = 1;
     }
  }
   fill->PID = pid_fill;
   PID = pid_fill;
//g Actualitzar el context d'execució del fill (eax = 0 i actualitzar kernel_esp
  ((union task_union*)fill)->stack[KERNEL_STACK_SIZE - 18] = &ret_from_fork;
  ((union task_union*)fill)->stack[KERNEL_STACK_SIZE - 19] = 0;
 // fill->kernel_esp = &((union task_union*)current())->stack[(getEbp() - (int) current())/sizeof(int) - 2];
  fill->kernel_esp = &((union task_union*)fill)->stack[KERNEL_STACK_SIZE-19];

//h
/*
  ((union task_union*)fill)->stack[(getEbp()-(int)current())/sizeof(int)] = (int) &ret_from_fork();
  ((union task_union*)fill)->stack[(getEbp() - (int) current())/sizeof(int)] = 0;
*/
//i Afegim el fill a la readyqueue
 list_add_tail(lliure, &readyqueue);
 printk("he fet el fork sencer");
//j retornem pid fill
  return PID;
}

void sys_exit()
{  
//free data structures and resources
//physical memory
//task struct
//and so  

struct task_struct * cur = current();
page_table_entry * c = get_PT(cur); 

//physical memory
for(int i = 0; i < NUM_PAG_DATA; i++){
	int frame = get_frame(c,PAG_LOG_INIT_DATA+i); 
	free_frame(frame); 
	del_ss_pag(c, PAG_LOG_INIT_DATA+i); 	

}

//free task struct 
current()->PID = -1;
current()->state = 3; 
current()->quantum = 0; 
//mirar si es pot esborrar el dir
list_add_tail(&(current()->list), &freequeue); 

//scheduler
			
}

int sys_gettime(){
	return zeos_ticks;
}

int sys_write(int fd, char * buffer, int size){
	int error; 
	if ((error = check_fd(fd, ESCRIPTURA)) < 0) return error;
	if (buffer == NULL) return -EFAULT;
	if (size < 0) return -EINVAL;


	char dest[32];
	int i = 0;
	int escrit = 0;
	while (i < size-32){
		copy_from_user(buffer+i,dest, 32);
		escrit += sys_write_console(dest, 32);
		i+=32;
	    }

	copy_from_user(buffer+i,dest, size-i);
	escrit += sys_write_console(dest, size-i); 
	return escrit;
} 

int sys_get_stats(int pid, struct stats *st){

	/*errors: PID no existe, PID < 0 */

	if(pid < 0) return -EINVAL; 
	int found = 0;
	for(int i = 0; i < NR_TASKS && !found; i++){
		if(task[i].task.PID == pid)
			found = 1; 
			copy_to_user(&task[i].task.stats,st, sizeof(struct stats));
			return 0; 
		}
	if (found == 0) return -ESRCH; 



}

# 1 "wrappers.S"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "wrappers.S"
# 1 "include/asm.h" 1
# 2 "wrappers.S" 2

.globl write; .type write, @function; .align 0; write:


 push %ebp;
 movl %esp, %ebp;
 pushl %ebx;
 pushl %edi;
 movl 8(%ebp), %ebx;
 movl 12(%ebp), %ecx;
 movl 16(%ebp), %edx;

 movl $4, %eax;


 push %ecx;
 push %edx;


 leal comprova_error, %edi;
 pushl %edi;


 pushl %ebp;
 movl %esp, %ebp;


 sysenter;

comprova_error:
 cmpl $0, %eax;
 jge end;

negatiu:
 neg %eax
 movl %eax, errno;
 movl $-1, %eax;
end:

 popl %ebp;
 popl %edi;
 popl %edx;
 popl %ecx;
 popl %edi;
 popl %ebx;
 popl %ebp;
 ret;


.globl gettime; .type gettime, @function; .align 0; gettime:

 pushl %ebp;
 movl %esp, %ebp;
 pushl %edi;
 movl $0x0a, %eax;

 pushl %ecx;
 pushl %edx;

 leal end_time, %edi;
 pushl %edi;
 pushl %ebp;
 movl %esp, %ebp;

 sysenter;

end_time:

 popl %ebp;
 popl %edi;
 popl %edx;
 popl %ecx;
 popl %edi;
 popl %ebp;


 ret;



.globl getpid; .type getpid, @function; .align 0; getpid:
 pushl %ebp;
 movl %esp, %ebp;

 movl $0x14, %eax;
 sysenter;
 popl %ebp;
 ret;

.globl fork; .type fork, @function; .align 0; fork:
 pushl %ebp;
 movl %esp,%ebp;

 movl $0x02, %eax;
 sysenter;
 pop %ebp;
 ret;


.globl exit; .type exit, @function; .align 0; exit:
 pushl %ebp;
 movl %esp,%ebp;

 movl $0x01, %eax;
 sysenter;
 pop %ebp;


.globl get_stats; .type get_stats, @function; .align 0; get_stats:
 pushl %ebp;
 movl %esp, %ebp
 push %ebx
 movl 8(%ebp), %ebx
 movl 12(%ebp), %ebx
 movl $0x35,%eax
 sysenter;
 pop %ebp;
